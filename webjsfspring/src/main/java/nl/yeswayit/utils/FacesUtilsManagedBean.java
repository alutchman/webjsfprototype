package nl.yeswayit.utils;

import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;

public final class FacesUtilsManagedBean {
    //=== force Singleton
    private FacesUtilsManagedBean(){

    }

    public static final <T> T getSpringBean(Class<T> aclass) {
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        ServletContext servletContext = (ServletContext) externalContext.getContext();
        return  WebApplicationContextUtils.getRequiredWebApplicationContext(servletContext).getBean(aclass);
    }
}
