package nl.yeswayit.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.jsf.el.SpringBeanFacesELResolver;

import javax.faces.context.FacesContext;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

/**
 * This class enables a n0-xml solution for the JSF-Spring application
 */
@Slf4j
public final class ELResolverInitializerServlet extends HttpServlet {
    private static final long serialVersionUID = 453966936150102307L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ELResolverInitializerServlet() {
        super();
    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        FacesContext context = FacesContext.getCurrentInstance();
        log.info("::::::::::::::::::: Faces context: " + context);
        context.getApplication().addELResolver(new SpringBeanFacesELResolver());
    }
}
