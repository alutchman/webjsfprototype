package nl.yeswayit.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Slf4j
@EnableWebMvc
@Configuration
public class ConfigInterceptDef implements WebMvcConfigurer {
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new LoggedInInterceptor()).addPathPatterns("/**").
                excludePathPatterns("/*index.html").
                excludePathPatterns("/*errors.html").
                excludePathPatterns("/*login.html").
                excludePathPatterns("/*check.html").
                excludePathPatterns("/");

        log.info("Added Interceptor: {}", LoggedInInterceptor.class.getCanonicalName());
    }
}
