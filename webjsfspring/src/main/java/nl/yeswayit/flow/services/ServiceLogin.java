package nl.yeswayit.flow.services;


import lombok.extern.slf4j.Slf4j;
import nl.yeswayit.jsonrest.data.repo.WebuserRepo;
import nl.yeswayit.jsonrest.data.tables.Webuser;
import nl.yeswayit.shared.transfer.SearchUser;
import nl.yeswayit.shared.transfer.UserLevel;
import nl.yeswayit.shared.transfer.WebUserData;
import nl.yeswayit.utils.SessionWallet;
import nl.yeswayit.utils.TransferUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;
import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class ServiceLogin {
    private static final String MAIN_ADMIN = "admin";
    private static final String INIT_PW = "welkom01";

    @Autowired
    private WebuserRepo daoWebuser;

    @PostConstruct
    public void init() throws NoSuchAlgorithmException {
        ensureAdmin();
    }

    public boolean login(String username, String password, HttpServletRequest request, HttpSession session ) {
        Optional<Webuser> optUser = daoWebuser.findById(username);
        if (optUser.isPresent()) {
            Webuser entity = optUser.get();
            String chkPassword = createHashData(password);
            String currentPassword = entity.getWachtwoord();
            if (chkPassword.equals(currentPassword)) {
                WebUserData webUserData = TransferUtils.fromWebuser(entity);
                session.invalidate();
                HttpSession newSession = request.getSession();
                newSession.setAttribute(WebUserData.class.getSimpleName(),webUserData);
                newSession.setAttribute(SessionWallet.class.getSimpleName(),new SessionWallet());
                return true;
            }
        }
        return false;
    }


    @Transactional
    public boolean wachtwoordNaInloggen(String oldPassword, String newPassword, WebUserData alterUser){
        Optional<Webuser> optUser = daoWebuser.findById(alterUser.getUserid());
        if (!optUser.isPresent()) {
            return false;
        }
        Webuser entity = optUser.get();

        if ( (oldPassword == null || oldPassword.trim().length() == 0 ) &&
             (newPassword == null || newPassword.trim().length() == 0 )  ){
            entity.setVoornaam(alterUser.getVoornaam());
            entity.setAchternaam(alterUser.getAchternaam());
        } else {
            entity.setVoornaam(alterUser.getVoornaam());
            entity.setAchternaam(alterUser.getAchternaam());
            entity.setWachtwoord(createHashData(newPassword.trim()));
        }
        return true;
    }

    @Transactional
    public void ensureAdmin()  {
        Optional<Webuser> entityOpt = daoWebuser.findById(MAIN_ADMIN);
        if (!entityOpt.isPresent()) {
            Webuser webuser = new Webuser();
            webuser.setUserid(MAIN_ADMIN);
            webuser.setGroupnaam(UserLevel.BEHEER.name());
            webuser.setAchternaam("Applicatie");
            webuser.setVoornaam("Beheer");
            webuser.setWachtwoord(createHashData(INIT_PW));
            daoWebuser.save(webuser);
            log.info("Admin registation.....");
        }
    }

    private String createHashData(String input)  {
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("MD5");
            md.update(input.getBytes());

            byte byteData[] = md.digest();

            String digested = DatatypeConverter
                    .printHexBinary(byteData).toUpperCase();
            return digested;
        } catch (NoSuchAlgorithmException e) {

            return null;
        }
    }


    @Transactional
    public void updateUser(WebUserData currentUser) {
        Optional<Webuser> entityOpt = daoWebuser.findById(currentUser.getUserid());

        Webuser entity = entityOpt.get();
        if (entity != null) {
            entity.setAchternaam(currentUser.getAchternaam());
            entity.setVoornaam(currentUser.getVoornaam());
            entity.setGroupnaam(currentUser.getCurrentlevel().name());
        }
    }

    public boolean isUserIdFree(WebUserData newUser) {
        Optional<Webuser> entity = daoWebuser.findById(newUser.getUserid());

        if (entity.isPresent()) {
            log.info("User {} is already registered ", newUser.getUserid());
            return false;
        }

        return true;
    }

    @Transactional
    public void addNewUser(WebUserData newUser) {
        Webuser webuser = new Webuser();
        webuser.setUserid(newUser.getUserid());
        webuser.setGroupnaam(newUser.getCurrentlevel().name());
        webuser.setAchternaam(newUser.getAchternaam());
        webuser.setVoornaam(newUser.getVoornaam());
        webuser.setWachtwoord(createHashData(INIT_PW));
        daoWebuser.save(webuser);
    }

    public List<WebUserData> haalGebruikersOp(SearchUser searchUser, String excudeUserId) {
        Optional<List<Webuser>> resultsOpt = daoWebuser.searchUser(searchUser.getVoornaam()+"%",
                searchUser.getAchternaam()+"%", excudeUserId);
        List<WebUserData> exportList = new ArrayList<>();
        if (resultsOpt.isPresent()) {
            List<Webuser> results = resultsOpt.get();
            for (Webuser entity : results) {
                exportList.add(TransferUtils.fromWebuser(entity));
            }
        }
        return exportList;
    }
}
