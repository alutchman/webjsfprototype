package nl.yeswayit.flow.frontend;

import lombok.Getter;
import lombok.Setter;
import nl.yeswayit.flow.services.SettingsService;
import nl.yeswayit.shared.transfer.WebUserData;
import nl.yeswayit.utils.FacesUtilsManagedBean;
import nl.yeswayit.utils.MenuDef;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@Getter
@Setter
@ManagedBean
@RequestScoped
public class MenuBean {
    private String contextPath;
    private final List<MenuDef> menuList = new ArrayList<>();

    private String logoutUrl;

    private String indexUrl;

    private int currentYear;

    private String appTitle;

    private transient SettingsService settingsService;

    @PostConstruct
    public void init() {
        settingsService = FacesUtilsManagedBean.getSpringBean(SettingsService.class);
        appTitle = settingsService.getCustomProperty("app.title");

        currentYear = Calendar.getInstance().get(Calendar.YEAR);
        contextPath = FacesContext.getCurrentInstance().getExternalContext().getApplicationContextPath();
        logoutUrl   = contextPath + "/logout.html";
        indexUrl    = contextPath + "/index.html";

        WebUserData webUserData = (WebUserData) FacesContext.getCurrentInstance().
                getExternalContext().getSessionMap().get(WebUserData.class.getSimpleName());
        if (webUserData == null) {
            menuList.add(new MenuDef("Inloggen", contextPath + "/index.html"));
            return;
        }
        /*
        if (webUserData.getCurrentlevel().ordinal() < 2) {
            menuList.add(new MenuDef("Registratie", contextPath + "/register.html"));
            if (webUserData.getCurrentlevel().ordinal() < 1) {
                menuList.add(new MenuDef("Locaties", contextPath + "/locaties.html"));
            }
            menuList.add(new MenuDef("Transacties", contextPath + "/transacties.html"));
        }
        if (webUserData.getCurrentlevel().ordinal() < 3) {
            menuList.add(new MenuDef("Rapporten", contextPath + "/report.html"));
        }
        */

        menuList.add(new MenuDef("Instellingen", contextPath + "/settings.html"));
    }
}
