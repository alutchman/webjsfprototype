package nl.yeswayit.flow.controllers;


import nl.yeswayit.flow.services.ServiceLogin;
import nl.yeswayit.shared.transfer.WebUserData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;

@Controller
public class AuthController {
    @Autowired
    private ServiceLogin serviceLogin;

    @Autowired
    private ApplicationContext applicationContext;


    @RequestMapping(value="check.html", method = RequestMethod.POST)
    public ModelAndView loginwithUsernaemandPassword(@RequestParam String username,
                                                     @RequestParam String password,
                                                     HttpServletRequest request, HttpSession session){
        Map<String, Object> datamap = new HashMap<String, Object>();
        if (serviceLogin.login(username, password, request,session)) {
            return new ModelAndView("redirect:/settings.html");
         } else {
            datamap.put("username", username);
            datamap.put("error", "Ongeldige Username of password");
            return new ModelAndView("auth/login" ,datamap);
        }
    }

    @RequestMapping(value="login.html", method = RequestMethod.GET)
    public ModelAndView loginGet(HttpServletRequest request, HttpServletResponse response){
        HttpSession session = request.getSession(false);
        if (session != null && session.getAttribute(WebUserData.class.getSimpleName()) != null){
            return new ModelAndView("redirect:/settings.html");
        } else {
            Map<String, Object> datamap = new HashMap<String, Object>();
            datamap.put("baseUrl", applicationContext.getApplicationName());
            return new ModelAndView("auth/login",datamap);
        }
    }

    @RequestMapping(value="logout.html", method = RequestMethod.POST)
    public ModelAndView logout(HttpSession session){
        if (session != null) {
            session.invalidate();
        }
        return new ModelAndView("redirect:/login.html");
    }

    @RequestMapping(value="index.html")
    public ModelAndView startup(HttpServletRequest request){
        return new ModelAndView("redirect:/login.html");
    }

    @RequestMapping(value="/")
    public String rootView(HttpServletRequest request){
        return "redirect:/index.html";
    }

    @RequestMapping(value = "errors.html")
    public ModelAndView renderErrorPage(HttpServletRequest httpRequest, RuntimeException ex) {
        Map<String, Object> datamap = new HashMap<String, Object>();
        datamap.put("baseUrl", applicationContext.getApplicationName());

        String errorMsg = "";
        int httpErrorCode = getErrorCode(httpRequest);

        datamap.put("code", httpErrorCode);

        switch (httpErrorCode) {
            case 400: {
                errorMsg = "Http Error Code: 400. Bad Request";
                break;
            }
            case 401: {
                errorMsg = "Http Error Code: 401. Unauthorized";
                break;
            }
            case 404: {
                errorMsg = "Http Error Code: 404. Resource not found ";
                break;
            }
            case 500: {
                errorMsg = "Http Error Code: 500. Internal Server Error";
                if (ex!= null && ex.getMessage() != null) {
                    errorMsg += ""+ ex.getMessage();
                }
                break;
            }
            default:{
                errorMsg = "Onbekende fout";
            }
        }
        datamap.put("errorMsg", errorMsg);
        ModelAndView errorPage = new ModelAndView("auth/error", datamap);
        return errorPage;
    }

    private int getErrorCode(HttpServletRequest httpRequest) {
        return (Integer) httpRequest
                .getAttribute("javax.servlet.error.status_code");
    }
}
