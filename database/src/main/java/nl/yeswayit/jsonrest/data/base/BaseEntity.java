package nl.yeswayit.jsonrest.data.base;


import javax.persistence.MappedSuperclass;
import java.io.Serializable;

@MappedSuperclass
abstract public  class BaseEntity implements Serializable {

}
