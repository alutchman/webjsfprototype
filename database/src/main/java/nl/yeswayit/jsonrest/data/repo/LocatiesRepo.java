package nl.yeswayit.jsonrest.data.repo;


import nl.yeswayit.jsonrest.data.tables.Locaties;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface LocatiesRepo  extends JpaRepository<Locaties, Long> {
    @Query("SELECT t FROM Locaties t where postcode like ?1 ")
    Optional<List<Locaties>> findLocatie(String postcode);

    @Query("SELECT t FROM Locaties t where postcode=?1 AND nummer=?2 AND toevoeging=?3")
    Optional<Locaties> findLocatie(String postcode, int huisnummer, String toevoeging);

    @Query("SELECT t FROM Locaties t where postcode=?1 AND nummer=?2")
    Optional<List<Locaties>> findLocaties(String postcode, int huisnummer);


}
