package nl.yeswayit.jsonrest.data;

import lombok.extern.slf4j.Slf4j;
import nl.yeswayit.shared.utils.JndiTemplateDataBase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.lookup.JndiDataSourceLookup;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.Properties;

@Slf4j
@Configuration
@ComponentScan("nl.yeswayit.jsonrest.data")
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = {
        "nl.yeswayit.jsonrest.data.repo"
})
public class DataAccessConfig {
    public static final String DATA_SOURCE_NAME = "jdbc/yeswayitTaxi";

    @Bean
    public DataSource getDataSource()  {
        JndiDataSourceLookup dsLookup = new JndiDataSourceLookup();
        dsLookup.setResourceRef(true);
        DataSource dsJndi =  dsLookup.getDataSource(DATA_SOURCE_NAME);
        log.info("Successs======================= JNDI, {}", DATA_SOURCE_NAME);
        return dsJndi;
    }

    @Bean
    public JndiTemplateDataBase fetchJndiTemplate(){
        JndiTemplateDataBase template = new JndiTemplateDataBase();
        return template;
    }

    @Bean
    @Autowired
    public JdbcTemplate createJdbCTemplate(DataSource dataSource){
        return new JdbcTemplate(dataSource);
    }


    @Bean
    @Autowired
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(DataSource dataSource, JndiTemplateDataBase jndiTemplateDataBase) {
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(dataSource);
        em.setPackagesToScan(new String[] {"nl.yeswayit.jsonrest.data.tables"});

        JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);
        try {
            String url = dataSource.getConnection().getMetaData().getURL();
           log.info("Datasource======================= URL,"+ url);
           if (url.startsWith("jdbc:h2:mem")) {
               Properties jpaProperties = new Properties();
               jpaProperties.setProperty("hibernate.dialect", "org.hibernate.dialect.H2Dialect");
               jpaProperties.setProperty("hibernate.show_sql", "true");
               jpaProperties.setProperty("hibernate.hbm2ddl.auto", "create-drop");
               jpaProperties.setProperty("hibernate.generate_statistics", "false");
               em.setJpaProperties(jpaProperties);
           } else {
               em.setJpaProperties(jndiTemplateDataBase.getEnvironment());
           }
        } catch (SQLException e) {

        }
        return em;
    }


    @Bean
    @Autowired
    public JpaTransactionManager transactionManager(EntityManagerFactory emf){
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(emf);

        return transactionManager;
    }

}
