# Vereisten
* Spring Boot version naar 2.4.2, waardoor boot niet meer werkt
* Werk niet in Intellij maar Moet gebruik maken van Tomcat op machine
* PrimeFace naar versie 8.0
* Werkt NIET met Java 8 en is ontwikkeld op Java 11
* Als build faalt:   mvn dependency:purge-local-repository


# Laatste Upgrades
* 1 Februari 2021

# DataBase MySql
* jdbc/yeswayitTaxi moet in in Tomcat staan
* JNDI gaat nu NIET meer via web.xml, maar staat in de config DataAccessConfig van de module **database**

Instructies voor deze  [markdown file](https://nl.wikipedia.org/wiki/Markdown)