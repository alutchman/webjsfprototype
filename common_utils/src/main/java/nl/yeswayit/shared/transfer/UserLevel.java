package nl.yeswayit.shared.transfer;

public enum UserLevel {
    BEHEER(true,true, true),ADMIN(false,true, true),REPORT(false,false,true),GEEN(false, false,false);

    private final boolean addUsers;
    private final boolean addTransactions;
    private final boolean viewReports;

    UserLevel(boolean addUsers, boolean addTransactions, boolean viewReports){
        this.addTransactions = addTransactions;
        this.addUsers = addUsers;
        this.viewReports = viewReports;
    }

    public boolean isAddUsers() {
        return addUsers;
    }

    public boolean isAddTransactions() {
        return addTransactions;
    }

    public boolean isViewReports() {
        return viewReports;
    }
}
