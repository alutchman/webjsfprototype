package nl.yeswayit.shared.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jndi.JndiTemplate;

import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;


public class JndiTemplateDataBase {
    private final String DB_RESOURCE = "dbase";
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private final JndiTemplate jndiTemplate;
    private final Properties environment;


    public static boolean isJUnitTest() {
        StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
        List<StackTraceElement> list = Arrays.asList(stackTrace);
        for (StackTraceElement element : list) {
            if (element.getClassName().startsWith("org.junit.")) {
                return true;
            }
        }
        return false;
    }


    public JndiTemplateDataBase(){
        jndiTemplate = new JndiTemplate();

        Properties props = new Properties();

        ResourceBundle labels = ResourceBundle.getBundle(DB_RESOURCE, Locale.getDefault());
        Enumeration bundleKeys = labels.getKeys();

        while (bundleKeys.hasMoreElements()) {
            String key = (String)bundleKeys.nextElement();
            String value = labels.getString(key);
            props.put(key, value);
        }
        jndiTemplate.setEnvironment(props);
        environment = jndiTemplate.getEnvironment();

    }


    public String getProperty(String key){
        return environment.getProperty(key);
    }

    public Properties getEnvironment() {
        return environment;
    }
}
