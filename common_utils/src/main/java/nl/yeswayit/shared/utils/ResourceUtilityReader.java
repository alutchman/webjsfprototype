package nl.yeswayit.shared.utils;

import java.util.Enumeration;
import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;

public final class ResourceUtilityReader {
    //==SingleTon
    private ResourceUtilityReader(){

    }

    public static Properties getProperties(String resource){
        Properties props = new Properties();

        ResourceBundle labels = ResourceBundle.getBundle(resource, Locale.getDefault());
        Enumeration bundleKeys = labels.getKeys();

        while (bundleKeys.hasMoreElements()) {
            String key = (String)bundleKeys.nextElement();
            String value = labels.getString(key);
            props.put(key, value);
        }
        return props;
    }
}
