package nl.yeswayit.shared.utils;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class JndiTemplateDataBaseTest {

    @Test
    public void isJUnitTest() {
        JndiTemplateDataBase jndiTemplateDataBase = new JndiTemplateDataBase();
        assertEquals("org.h2.Driver", jndiTemplateDataBase.getProperty("jdbc.driverClassName"));
        assertEquals("jdbc:h2:mem:yeswayit_test", jndiTemplateDataBase.getProperty("jdbc.url"));
    }
}